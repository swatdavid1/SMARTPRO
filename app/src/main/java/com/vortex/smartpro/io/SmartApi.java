package com.vortex.smartpro.io;

import com.vortex.smartpro.models.Coordenates;
import com.vortex.smartpro.models.Responsejson;
import com.vortex.smartpro.models.Send;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface SmartApi {

    @GET("ApiAppSmart")
    Call<Responsejson> getdata();

    @PUT("ApiUpdateStatus/{id}/{safe}")
    @FormUrlEncoded
    Call<Send> Send_alert (@Path("id") int id,
                           @Path("safe") int alert,
                           @Field("status")int aux);

    @PUT("ApiUpdateCoordinates/{id}/{lat}/{lon}")
    @FormUrlEncoded
    Call<Coordenates> Send_direction (@Path("id") int id,
                                      @Path("lat")Double lat,
                                      @Path("lon")Double lon,
                                      @Field("status")int aux);
}
