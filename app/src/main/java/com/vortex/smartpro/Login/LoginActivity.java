package com.vortex.smartpro.Login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vortex.smartpro.MainActivity;
import com.vortex.smartpro.R;
import com.vortex.smartpro.io.ApiAdapter;
import com.vortex.smartpro.models.DispSmart;
import com.vortex.smartpro.models.Responsejson;
import com.vortex.smartpro.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private AutoCompleteTextView smartView;
    private EditText edeView;
    private View mProgressView;
    private View mLoginFormView;
    private String ede1;
    private String mEde;
    private String smart1;
    private String mSmart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        smartView = (AutoCompleteTextView) findViewById(R.id.smart);
        edeView = (EditText) findViewById(R.id.ede);

        Button mEmailSignInButton = (Button) findViewById(R.id.verifyauth);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
                Snackbar.make(view, "Verficando credenciales", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void attemptLogin() {
        // Reset errors.
        smartView.setError(null);
        edeView.setError(null);

        // Store values at the time of the login attempt.
        final String smart = smartView.getText().toString();
        final String ede = edeView.getText().toString();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(ede) && isPasswordInValid(ede)) {
            edeView.setError(getString(R.string.error_invalid_password));
            focusView = edeView;
            cancel = true;
        }

        if (TextUtils.isEmpty(ede)){
            edeView.setError(getString(R.string.error_field_required));
            focusView = edeView;
            cancel = true;
        }else if (ede.length() == 7){
            ede1 = ede.substring(0, 1);
            mEde = ede.substring(1, 2);
        }else{
            edeView.setError(getString(R.string.error_invalid_password));
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(smart)) {
            smartView.setError(getString(R.string.error_field_required));
            focusView = smartView;
            cancel = true;
        } else if (isEmailInValid(smart)) {
            smartView.setError(getString(R.string.error_invalid_email));
            focusView = smartView;
            cancel = true;
        }

        if (smart.length() == 7){
            smart1 = smart.substring(0, 1);
            mSmart = smart.substring(4, 5);
        }

        if (smart1!=null && !smart1.equals("S")||mSmart!=null && !mSmart.equals("m")) {
            smartView.setError(getString(R.string.incorrect_smart_code));
            focusView = smartView;
            cancel = true;
        }
        if (ede1!=null && !ede1.equals("E")||mEde!=null && !mEde.equals("d")){
            edeView.setError(getString(R.string.incorrect_ede_code));
            focusView = edeView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            Call<Responsejson> users = ApiAdapter.getApiService().getdata();
            users.enqueue(new Callback<Responsejson>() {
                @Override
                public void onResponse(Call<Responsejson> call, Response<Responsejson> response) {
                    if(response!=null&&response.isSuccessful()){
                        Responsejson responsejson = response.body();
                        List<User> user = responsejson.getUsers();
                        for(int i = 0; i< user.size(); i++){
                            List<DispSmart> dispSmart = user.get(i).getDispSmart();
                            for(int j = 0; j<dispSmart.size(); j++){
                                String serialS = user.get(i).getDispSmart().get(j).getNumeroSerial();
                                String serialE = user.get(i).getDispSmart().get(j).getNumeroSerialE();
                                if(serialS.equals(smart)&&serialE.equals(ede)){
                                    SaveRemember.get(LoginActivity.this).saveUser(responsejson,i,j);
                                    Intent intent1 = new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(intent1);
                                    finish();
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Responsejson> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),"Sin respuesta, intente nuevamente",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getBaseContext(),LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

        }
    }

    private boolean isEmailInValid(String smart) {
        //TODO: Replace this with your own logic
        return smart.length() != 7;
    }

    private boolean isPasswordInValid(String ede) {
        //TODO: Replace this with your own logic
        return ede.length() != 7;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

     /*   mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });*/

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}

