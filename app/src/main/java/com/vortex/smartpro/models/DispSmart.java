package com.vortex.smartpro.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DispSmart {

    @SerializedName("numero_serial")
    @Expose
    private String numeroSerial;
    @SerializedName("numero_serial_e")
    @Expose
    private String numeroSerialE;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getNumeroSerial() {
        return numeroSerial;
    }

    public void setNumeroSerial(String numeroSerial) {
        this.numeroSerial = numeroSerial;
    }

    public String getNumeroSerialE() {
        return numeroSerialE;
    }

    public void setNumeroSerialE(String numeroSerialE) {
        this.numeroSerialE = numeroSerialE;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
