package com.vortex.smartpro.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("dispSmart")
    @Expose
    private List<DispSmart> dispSmart = null;

    public User(Integer id, String username){
        this.username = username;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<DispSmart> getDispSmart() {
        return dispSmart;
    }

    public void setDispSmart(List<DispSmart> dispSmart) {
        this.dispSmart = dispSmart;
    }

}
