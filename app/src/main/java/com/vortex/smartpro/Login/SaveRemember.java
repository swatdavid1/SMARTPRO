package com.vortex.smartpro.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.vortex.smartpro.models.Responsejson;

public class SaveRemember {

    private final SharedPreferences sharedPreferences;

    private boolean mIsLoggedIn = false;

    private static SaveRemember INSTANCE;

    public static SaveRemember get(Context context){
        if (INSTANCE == null){
            INSTANCE = new SaveRemember(context);
        }
        return INSTANCE;
    }

    private SaveRemember(Context context) {
        sharedPreferences = context.getApplicationContext()
                .getSharedPreferences("SAVE_NAMES", Context.MODE_PRIVATE);

        mIsLoggedIn = !TextUtils.isEmpty(sharedPreferences.getString("SAVE_SMARTS", null));
    }

    public boolean isLoggedIn(){
        return mIsLoggedIn;
    }

    public void saveUser(Responsejson remember, int i, int j){
        if(remember != null){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("SAVE_USERNAMES", remember.getUsers().get(i).getUsername());
            editor.putString("SAVE_SMARTS", remember.getUsers().get(i).getDispSmart().get(j).getNumeroSerial());
            editor.putString("SAVE_EDES",remember.getUsers().get(i).getDispSmart().get(j).getNumeroSerialE());
            editor.putInt("ID_DISPS",remember.getUsers().get(i).getDispSmart().get(j).getId());
            editor.apply();

            mIsLoggedIn = true;
        }
    }


    public void logOut(){
        mIsLoggedIn = false;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SAVE_USERNAMES", null);
        editor.putString("SAVE_SMARTS", null);
        editor.putString("SAVE_EDES", null);
        editor.putInt("ID_DISPS", 0);
        editor.apply();
    }
}