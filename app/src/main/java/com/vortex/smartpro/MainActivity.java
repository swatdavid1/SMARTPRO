package com.vortex.smartpro;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.vortex.smartpro.Login.LoginActivity;
import com.vortex.smartpro.Login.SaveRemember;
import com.vortex.smartpro.io.ApiAdapter;
import com.vortex.smartpro.models.Coordenates;
import com.vortex.smartpro.models.Send;
import com.vortex.smartpro.services.JobIntentService;
import com.vortex.smartpro.services.WebSocketService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainActivity extends AppCompatActivity {

    private TextView alert;
    private TextView usename;
    private Button GPS;
    private Button safe;
    private ImageButton logout;
    private Button change;
    private ImageButton link;
    private int sig;
    private Double latitude;
    private Double longitude;
    private String Direccion;
    boolean gpsEnabled;
    boolean netEnabled;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    LocationRequest Localizacion;
    FusedLocationProviderClient mFusedLocationClient;
    Dialog customDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usename = findViewById(R.id.username);
        mFusedLocationClient = getFusedLocationProviderClient(this);
        startLocationUpdates();

        if (!SaveRemember.get(this).isLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else{
            SharedPreferences preferences = getSharedPreferences("SAVE_NAMES",Context.MODE_PRIVATE);
            String name = preferences.getString("SAVE_USERNAMES","x");
            usename.setText(name);
            int signal = preferences.getInt("ID_DISPS",0);
            sig = signal;
        }


        GPS = findViewById(R.id.GPS);
        GPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ReviewGoogleService("com.google.android.gms", getApplicationContext())){
                    checkLocationPermission();
                }
            }
        });

        alert = findViewById(R.id.Alerta);
        alert.setText(R.string.Alert);
        alert.setTextColor(getResources().getColor(R.color.red));

        safe = findViewById(R.id.Safe);
        safe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendSignal();
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveRemember.get(MainActivity.this).logOut();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();
            }
        });


        link = findViewById(R.id.linked);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.vortexrobotic.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        change = (Button) findViewById(R.id.news);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeToNews();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this, JobIntentService.class);
            startService(intent);
        } else {
            Intent local = new Intent(this, WebSocketService.class);
            startService(local);
        }
    }

    public void changeToNews(){
        Intent intent = new Intent(MainActivity.this,NewsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private void Dialog(String Content, String title) {
        customDialog = new Dialog(this, R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.dialog_design);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText(title);

        TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        contenido.setText(Content);

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    public void onBackPressed(){
        Dialog(getString(R.string.exit),getString(R.string.question));
    }

    private void SendSignal() {
        int signal = 0;
        Call<Send> data = ApiAdapter.getApiService().Send_alert(sig,signal,signal);
        data.enqueue(new Callback<Send>() {
            @Override
            public void onResponse(Call<Send> call, Response<Send> response) {
                Dialog(getString(R.string.content),getString(R.string.title));
            }

            @Override
            public void onFailure(Call<Send> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se ha podido establecer conexion con el servidor",
                        Toast.LENGTH_LONG).show();

            }
        });
    }

    private void SendCoordenates(){
        Toast.makeText(getApplicationContext(),"Enviando ubicacion...", Toast.LENGTH_SHORT).show();
        Call<Coordenates> coordenatesCall = ApiAdapter.getApiService().Send_direction(sig,latitude,longitude,1);
        coordenatesCall.enqueue(new Callback<Coordenates>() {
            @Override
            public void onResponse(Call<Coordenates> call, Response<Coordenates> response) {
                Dialog(getString(R.string.response) +". Direccion enviada: \n " + Direccion,getString(R.string.Ubicate));
            }

            @Override
            public void onFailure(Call<Coordenates> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"No se ha podido enviar su direccion, intente nuevamente",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean ReviewGoogleService(String nombrePaquete, Context context) {

        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        Toast.makeText(getApplicationContext(),"Verificando permisos", Toast.LENGTH_SHORT).show();
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert mlocManager != null;
        gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        netEnabled = mlocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }

        if (netEnabled) {
            mFusedLocationClient.requestLocationUpdates(Localizacion, mLocationCallback, Looper.myLooper());
        } else if (gpsEnabled) {
            mFusedLocationClient.requestLocationUpdates(Localizacion, mLocationCallback, Looper.myLooper());
        }
        if (!gpsEnabled) {
            int hasWriteContactsPermission = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to GPS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    return;
                }
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }

            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode > 0) { //decia == 1000 en caso de revertir los cambios.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               // if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    checkLocationPermission();
                //    return;
               // }
                //mFusedLocationClient.requestLocationUpdates(Localizacion, mLocationCallback, Looper.myLooper());
            }
        }
    }

    protected void startLocationUpdates() {

        Localizacion = new LocationRequest();
        Localizacion.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Localizacion.setInterval(9000);
        Localizacion.setFastestInterval(8000);
        Localizacion.setNumUpdates(1);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(Localizacion);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        Looper.myLooper();
    }

    public void setLocation(android.location.Location loc) {
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            latitude = loc.getLatitude();
            longitude = loc.getLongitude();
          //  Toast.makeText(getApplicationContext(),"Ubicacion obtenida", Toast.LENGTH_SHORT).show();
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    Direccion = DirCalle.getAddressLine(0);
                    SendCoordenates();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<android.location.Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                android.location.Location loc = locationList.get(locationList.size()- 1);
                loc.getLatitude();
                loc.getLongitude();
                setLocation(loc);
            }
        }
    };
}

